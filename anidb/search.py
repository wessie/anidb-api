"""Module to access the anidb HTTP API

"""

from anime import Anime
from __init__ import find_or_create_directory, urlopen
from whoosh.index import open_dir, create_in, EmptyIndexError
from whoosh.qparser.default import QueryParser
from whoosh.filedb.multiproc import MultiSegmentWriter
from multiprocessing import cpu_count

version = 0.5

api_version = 0.1
api_clientver = 1
api_client = "amazingweeaboo"
api_protover = 1

database_url = "http://anidb.net/api/animetitles.dat.gz"

class Index(object):
    """An object representing the index of anime in the current anidb database
        available on local disk"""
    _q_aid = u"id:{aid}"
    _q_kind = u"kind:{kind}"
    _q_lang = u"language:{language}"
    _q_query = u"title:{title}"
    kinds = {"primary": u"1", "synonym": u"2", "short": u"3", "official": u"4"}
    def __init__(self):
        object.__init__(self)
        self.index_dir = find_or_create_directory("index")
        try:
            self.index = open_dir(self.index_dir)
        except (EmptyIndexError):
            from whoosh.fields import Schema, TEXT, NUMERIC, KEYWORD
            schema = Schema(title=TEXT(stored=True),
                    kind=NUMERIC(stored=True),
                    language=KEYWORD(stored=True),
                    id=NUMERIC(stored=True))
            self.index = create_in(self.index_dir, schema)
        self._parser = QueryParser("title", self.index.schema)
        self._searcher = self.index.searcher()
        
    def search(self, title, language="x-jat", kind="primary"):
        """Searches the database for 'query' that is in language 'language' and
        is of kind 'kind'
        
        """
        query = [self._q_query]
        if (not kind is None):
            query.append(self._q_kind)
            kind = self.kinds[kind]
        if (not language is None):
            query.append(self._q_lang)
        return Anime.from_result(self.raw(u" ".join(query).format(**locals())))
    
    def get(self, aid, language=None, kind=u"primary"):
        """Return the Anime associated with the ID 'aid'"""
        query = [self._q_aid]
        if (not kind is None):
            query.append(self._q_kind)
            kind = self.kinds[kind]
        if (not language is None):
            query.append(self._q_lang)
        return Anime.from_result(self.raw(u" ".join(query).format(**locals())))
    
    def raw(self, query):
        """Performs a raw query on the whoosh database, it's better to use the
        specialized methods for searching"""
        if (not isinstance(query, unicode)):
            query = query.decode("utf-8")
        return self._searcher.search(self._parser.parse(query))
    
    def refresh(self):
        """Refreshes the index, this does nothing if the index is up-to-date"""
        self._searcher = self._searcher.refresh()
        
    def update(self):
        """Updates the database with a new copy of the anidb database
        
        this method can take a while to complete and is blocking"""
        writer = MultiSegmentWriter(self.index, procs=cpu_count())
        try:
            import gzip
            fileobj = gzip.GzipFile(filename="F:/animetitles.dat.gz")
        except (IOError):
            fileobj = urlopen(database_url)
        try:
            for line in fileobj:
                if (line.strip()[:1] != "#"):
                    line = line.decode("utf-8", "replace")
                    line = line.split(u"|")
                    writer.add_document(id=line[0],
                                        kind=line[1],
                                        language=line[2],
                                        title=line[3].strip())
        finally:
            fileobj.close()
            writer.commit()
            self.refresh()

    def count(self):
        return self.index.doc_count()
    
    def modified(self):
        return self.index.last_modified()