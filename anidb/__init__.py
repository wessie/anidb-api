import errno

__all__ = ["search", "anime"]

try:
    WindowsError
except NameError:
    class WindowsError(Exception):
        pass
        
def find_or_create_directory(name="index"):
    """Finds the directory that holds our stuff, creates it if it doesn't 
        exist in the place you expect it to be in"""
    import os.path, sys
    cwd = os.path.realpath(os.path.dirname(sys.argv[0]))
    if (cwd.endswith("anidb")):
        # We are inside an anidb directory
        result = os.path.join(cwd, name)
    else:
        result = os.path.join(cwd, "anidb/" + name)
    try:
        os.makedirs(result)
    except (WindowsError, IOError, OSError) as err:
        if (err.errno == errno.EEXIST):
            pass
        else:
            raise
    except (os.error):
        raise
    return result
    
def urlopen(url):
    """Uses urllib2 to connect and retrieve a url with gzip encoding
    supported.
    
    usage is simple "urlopen("http://your-url.info")" returns a file-like object
    
    will return a normal urllib2.urlopen() response if content isn't gzipped
    """
    import urllib2
    import StringIO
    import gzip
    request = urllib2.Request(url)
    request.add_header("Accept-Encoding", "gzip")
    conn = urllib2.urlopen(request)
    if (conn.info().get("Content-Encoding") == "gzip"):
        f = gzip.GzipFile(fileobj=StringIO.StringIO(conn.read()))
        conn.close()
        return f
    return conn