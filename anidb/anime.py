from __init__ import find_or_create_directory, urlopen
import os
import gzip
import time

try:
    from xml.etree import cElementTree as ElementTree
except (ImportError):
    from xml.etree import ElementTree
    
class AnimeError(BaseException):
    pass

class AnimeFileError(AnimeError):
    pass

class Anime(object):
    settings = {"clientver": 1,
                "client": "amazingweeaboo",
                "protover": 1,
                "url": "http://api.anidb.net:9001/httpapi?"\
                        "request=anime"\
                        "&client={client}"\
                        "&clientver={clientver}"\
                        "&protover={protover}&aid={aid}"}
    last_request = time.time()-2.0
    def __init__(self, aid):
        object.__init__(self)
        self.aid = aid
        
    def __str__(self):
        return "<Anime id: " + self.aid + ">"
    def __repr__(self):
        return self.__str__()
    def get(self, key, default=None):
        """Simple method that gets rid of a load of code duplicates
        """
        try:
            return self.__dict__[key]
        except (KeyError):
            try:
                value = self.xml.find(key).text
            except (AttributeError):
                return default
            else:
                self.__dict__[key] = value
                return value
    @property
    def xml(self):
        xml = self.__dict__.get("xml", None)
        if (not xml):
            self.retrieve()
            xml = self.__dict__.get("xml", None)
        return xml

    @property
    def image(self):
        """Image associated with this Anime"""
        return self.get("picture", None)
    
    @property
    def end(self):
        """Date this anime finished airing"""
        return self.get("enddate", None)
    
    @property
    def start(self):
        """Date this anime started airing"""
        return self.get("startdate", None)
    
    @property
    def description(self):
        """Description given to this Anime, can be seen as synopsis"""
        return self.get("description", None)
    
    @property
    def type(self):
        """The kind of Anime, i.e. Movie, TV Series, others"""
        return self.get("type", None)
    
    @property
    def episode_count(self):
        """The amount of episodes that are available of this Anime"""
        return self.get("episodecount", None)
    
    @property
    def title(self):
        """A dictionary of titles, all languages"""
        try:
            return self.__dict__["title"]
        except KeyError:
            titles = Titles(self.xml.find("titles"))
            self.__dict__["title"] = titles
            return titles
        
    @property
    def categories(self):
        try:
            categories = self.__dict__["categories"]
        except (KeyError):
            categories = [Category(element) for element in \
                              self.xml.find("categories")]
            self.__dict__["categories"] = categories
        return categories
    @property
    def tags(self):
        try:
            tags = self.__dict__["tags"]
        except (KeyError):
            tags = [Tag(element) for element in \
                              self.xml.find("tags")]
            self.__dict__["tags"] = tags
        return tags
    def retrieve(self):
        """Retrieve XML file from anidb API"""
        
        def read_file(aid):
            """Read an anime XML file from disk"""
            filename = self.anime_data_path(aid)
            if (filename != None):
                try:
                    fileobj = gzip.GzipFile(filename=self.anime_data_path(aid))
                except (IOError):
                    raise
                else:
                    return fileobj
            else:
                raise TypeError("Filename can't be none")
            
        def write_file(fileobj, aid):
            """Write an anime XML file to disk"""
            fileobj.seek(0)
            with gzip.open(filename=self.anime_data_path(aid, send_path=True),
                                                        mode="w") as target:
                target.writelines(fileobj)
                
        try:
            if (self.anime_data_path(self.aid)):
                data = read_file(self.aid)
                try:
                    self.__dict__['xml'] = ElementTree.parse(data)
                except (ElementTree.ParseError):
                    raise AnimeFileError("XML failed to parse")
            else:
                raise AnimeFileError("File does not exist")
        except (AnimeFileError):
            if (time.time() - self.last_request <= 2.0):
                time.sleep(time.time() - self.last_request)
            self.last_request = time.time()
            data = urlopen(self.settings['url'].format(aid=self.aid,
                                                       **self.settings))
            try:
                self.__dict__["xml"] = ElementTree.parse(data)
            except (ElementTree.ParseError):
                raise AnimeFileError("XML failed to parse")
            finally:
                write_file(data, self.aid)
    @staticmethod
    def anime_data_path(aid, send_path=False):
            path = os.path.join(find_or_create_directory("cache"), "anime" + str(aid))
            if (os.path.exists(path)):
                # We know we have a file
                return path
            return path if send_path else None
    def __getstate__(self):
        return self.aid
    def __setstate__(self, state):
        self.aid = state
    @classmethod
    def from_result(cls, result_set):
        anime = []
        for result in result_set:
            anime.append(cls(result["id"]))
        return anime
class Base(object):
    namespace = "{http://www.w3.org/XML/1998/namespace}"
    def __init__(self, element):
        object.__init__(self)
        self.xml = element
    def text(self, key, default=None):
        try:
            return self.xml.find(key).text
        except (AttributeError):
            return default
    def __str__(self):
        return "<Base of " + str(self.xml) + ">"
    def __repr__(self):
        return self.__str__()

class Titles(Base):
    def __init__(self, element):
        Base.__init__(self, element)
        for element in self.xml.iter("title"):
            lang = element.get(self.namespace + "lang")
            self.__dict__.setdefault(lang, []).append({"lang": lang,
                                   "title": element.text,
                                   "type": element.get("type")})
    def __getitem__(self, key):
        return self.__dict__[key]
    def __iter__(self):
        for key, value in self.__dict__.iteritems():
            if (key == "xml"):
                continue
            for title in value:
                yield title
class Category(Base):
    @property
    def description(self):
        """Description of this category"""
        return self.text("description", "No description available")
    @property
    def name(self):
        """Name of this category"""
        return self.text("name", None)
    @property
    def id(self):
        """Id of this category"""
        return self.xml.get("id", None)
    @property
    def parentid(self):
        return self.xml.get("parentid", None)
    @property
    def hentai(self):
        hentai = self.xml.get("hentai", "false")
        return False if hentai == "false" else True
    @property
    def weight(self):
        """The relevance to the anime of this category"""
        return self.xml.get("weight", 0)
    def __str__(self):
        return "<Category '{name}' at {hex}>".format(
                                                  name=self.name,
                                                  hex=hex(id(self))
                                                  )
        
class Tag(Base):
    @property
    def description(self):
        """Description of this category"""
        return self.text("description", "No description available")
    @property
    def name(self):
        """Name of this category"""
        return self.text("name", None)
    @property
    def count(self):
        """Count??? no idea"""
        return self.text("count", "0")
    @property
    def id(self):
        return self.xml.get("id", None)
    @property
    def approval(self):
        return self.xml.get("approval", None)
    @property
    def spoiler(self):
        return False if self.xml.get("spoiler", "true") else True
    @property
    def localspoiler(self):
        return False if self.xml.get("localspoiler", "true") else True
    @property
    def globalspoiler(self):
        return False if self.xml.get("globalspoiler", "true") else True
    @property
    def update(self):
        return self.xml.get("update", None)
    def __str__(self):
        return "<Tag '{name}' at {hex}>".format(
                                                  name=self.name,
                                                  hex=hex(id(self))
                                                  )
