﻿import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
      name="anidb-api",
      version="0.1.0alpha",
      author='Wessie',
      author_email='anidb@wessie.info',
      description=("""A simple Python API for anidb with integrated local search."""),
      license='GPL',
      long_description=read('README.rst'),
      install_requires=[
                  "whoosh",
                          ],
      keywords="anime anidb api",
      packages=['anidb'],
      )