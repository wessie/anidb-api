﻿===========
Description
===========

**anidb-api** is a simple (and incomplete) API for `anidb.net` with local search support.

..note::
    This is untested and in early alpha stage code. It's usable by low standards.

============
Installation
============

Installation should be as easy as doing a:
    
    `python setup.py install`
    
after which you can do `import anidb` to use the module.

============
Dependencies
============

**anidb-api** depends on the excellent **whoosh** search library written in Pure Python for
local search support.